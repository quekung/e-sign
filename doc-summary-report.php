<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1>E-Sign</h1>
						<!--<div class="top-right-button-container">
                        <a href="manual-keyin.php" class="btn btn-success top-right-button rounded-05 mr-1"><i class="icon-img"><img src="di/ic-add-plus.png" height="16"></i> MANUAL KEY-IN</a>
						<a href="import-estamp-upload.php" class="btn btn-blue top-right-button rounded-05 mr-1"><i class="icon-img"><img src="di/ic-upload.png" height="16"></i> UPLOAD  FILE</a>

                       
                    </div>-->
					
						<nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>
								<li class="breadcrumb-item">
									<a href="#">Library</a>
								</li>-->
								<li class="breadcrumb-item active text-gray" aria-current="page">Document Summary Report</li>
							</ol>
						</nav>

                    </div>

                    <div class="srh-bar mb-4 d-flex justify-content-between flex-row flex-nowrap">
						<div class="col p-0 pl-1 pr-0">
							<a class="btn p-2 d-inline-block d-md-none" data-toggle="collapse" href="#searchOptions"
								role="button" aria-expanded="true" aria-controls="searchOptions">
								Display Options
								<i class="simple-icon-arrow-down align-middle"></i>
							</a>
							<div class="collapse d-md-block" id="searchOptions">
								<div class="d-flex flex-wrap justify-content-between h-100">
									<div class="main-srh col-xs-12 col-sm p-0 mr-3 mb-1">

										<div class="input-group">
											
											<input type="text" class="form-control bg-transparent" placeholder="Search by Certificate name, Application name">
											<div class="input-group-append position-absolute">
												<button type="button" class="btn bg-transparent border-0"><img src="di/ic-search.png" height="22"></button>
											</div>
										</div>
									</div>

																		
									<div class="float-md-left d-flex align-items-center">
										
										<button class="btn btn-outline-warning btn-md top-right-button rounded-05 mr-1" type="button" id="filterMenuButton" data-toggle="collapse" href="#filterMenuDropdown" role="button" aria-expanded="false" aria-controls="filterMenuDropdown"> <i class="icon-img"><img src="di/ic-filter.png" height="20"></i> FILTER LIST</button>
										<div class="position-absolute collapse multi-collapse" id="filterMenuDropdown">
											<div class="card mb-4">
												<div class="card-body p-3">
													<h5 class="h6 mb-2">Schedule Date</h5>

													<div class="row mb-3">
														<div class="col-6 pr-2">
															<div class="input-group date">
																	<span class="input-group-text input-group-append input-group-addon">
																		<i class="simple-icon-calendar"></i>
																	</span>
																	<input type="text" class="input-sm form-control" name="start" placeholder="Start date" />
																</div>

														</div>
														<div class="col-6 pl-2">
															<div class="input-group date">
																	<span class="input-group-text input-group-append input-group-addon">
																		<i class="simple-icon-calendar"></i>
																	</span>
																	<input type="text" class="input-sm form-control" name="end" placeholder="End date" />
																</div>
														</div>
													</div>
													
													<h5 class="h6 mb-2">Import Date</h5>
													<div class="row mb-3">
														<div class="col-6 pr-2">
															<div class="input-group date">
																	<span class="input-group-text input-group-append input-group-addon">
																		<i class="simple-icon-calendar"></i>
																	</span>
																	<input type="text" class="input-sm form-control" name="start" placeholder="Start date" />
																</div>

														</div>
														<div class="col-6 pl-2">
															<div class="input-group date">
																	<span class="input-group-text input-group-append input-group-addon">
																		<i class="simple-icon-calendar"></i>
																	</span>
																	<input type="text" class="input-sm form-control" name="end" placeholder="End date" />
																</div>
														</div>
													</div>
													
													<h5 class="h6 mb-2">Job Finished Date</h5>
													<div class="row mb-3">
														<div class="col-6 pr-2">
															<div class="input-group date">
																	<span class="input-group-text input-group-append input-group-addon">
																		<i class="simple-icon-calendar"></i>
																	</span>
																	<input type="text" class="input-sm form-control" name="start" placeholder="Start date" />
																</div>

														</div>
														<div class="col-6 pl-2">
															<div class="input-group date">
																	<span class="input-group-text input-group-append input-group-addon">
																		<i class="simple-icon-calendar"></i>
																	</span>
																	<input type="text" class="input-sm form-control" name="end" placeholder="End date" />
																</div>
														</div>
													</div>

													<div class="row">
														
														<div class="col-sm-12 mb-3">
															<label class="hid">Certificate Namet</label>
															<select class="form-control select2-normal" data-width="100%" data-placeholder="Certificate Name">
																<option></option>
																<option>option 1</option>
																<option>option 2</option>
															</select>
														</div>

														<div class="col-sm-12 mb-3">
															<label class="hid">Channel</label>
															<select class="form-control select2-normal" data-width="100%" data-placeholder="Channel">
																<option></option>
																<option>option 1</option>
																<option>option 2</option>
															</select>
														</div>

														<div class="col-sm-12 mb-3">
															<label class="hid">Application Name</label>
															<select class="form-control select2-normal" data-width="100%" data-placeholder="Application Name">
																<option></option>
																<option>option 1</option>
																<option>option 2</option>
															</select>
														</div>
														
														<div class="col-sm-12 mb-3">
															<label class="hid">Status</label>
															<select class="form-control select2-normal" data-width="100%" data-placeholder="Status">
																<option></option>
																<option>option 1</option>
																<option>option 2</option>
															</select>
														</div>														
													</div>

													<div class="row">
														<div class="col-6 pr-2">
															<button type="button" class="btn btn-block btn-outline-primary rounded-05">ยกเลิก</button>

														</div>
														<div class="col-6 pl-2">
															<button type="button" class="btn btn-block btn-primary rounded-05">ยืนยัน</button>
														</div>
													</div>


												</div>
											</div>

										</div>
									</div>

								</div>
							</div>
						</div>
						
					</div>


					<div class="card">
					<div class="card-body p-0">
						<div class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">

                           <!--<table class="data-table dataTable no-footer responsive nowrap table-responsive-lg" >-->
						   <table  class="data-table data-table-scrollable responsive nowrap skin-tmb">
							
									<thead>
										<tr>
											<th class="text-white sort-none text-center">No.</th>
											<th class="text-white sort-none">Schedule Time</th>
											<th class="text-white sort-none">Job Start</th>
											<th class="text-white sort-none">Job Finished</th>
											<th class="text-white sort-none">Total</th>
											<th class="text-white sort-none">Suscess</th>
											<th class="text-white sort-none">Fail</th>
											<th class="text-white sort-none">Certificate Name </th>
											<th class="text-white sort-none">Channel</th>
											<th class="text-white sort-none">Application Name</th>
											<th class="text-white sort-none">Status</th>
											<th class="text-white sort-none">Action</th>
										</tr>
									</thead>
									<tbody>
										<?php for($i=1;$i<=10;$i++){ ?>
										<tr>
											<td class="text-center pr-4"><?php echo $i ?></td>
											<td>15/10/21 17:00</td>
											<td>
												15/10/21 17:00
											</td>
											<td>15/10/21 17:00</td>
											<td>100</td>
											<td>90</td>
											<td>10</td>
											<td>TMB.co.th</td>
											<td>Batch</td>
											<td>ALS</td>
											<td>Done In Process Waiting</td>
											<td class="text-center">
												<a href="#" class="btn  btn-xs p-1" title="View"><i class="icon-img"><img src="di/ic-view.png" height="16"></i></a> 
											</td>
										</tr>
										<?php } ?>
							

									</tbody>
								</table>
								
		

						</div>
						
						<div class="mt-4">
                 
						<div class="d-flex justify-content-end align-items-center">
								<div class="dropdown-as-select display-page" id="pageCount">
									<span class="text-light text-small">Rows per page </span>
									<button class="btn btn-outline-dark btn-xs dropdown-toggle" type="button"
										data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										10
									</button>
									<div class="dropdown-menu dropdown-menu-right">
										<a class="dropdown-item" href="#">5</a>
										<a class="dropdown-item active" href="#">10</a>
										<a class="dropdown-item" href="#">20</a>
									</div>
								</div>
								<div class="d-block d-md-inline-block ml-5">
									<nav class="ctrl-page d-flex flex-nowrap align-items-center">
										<span> 1-10 of 40</span>
										<ul class="pagination justify-content-center mb-0">
										   <!-- <li class="page-item ">
												<a class="page-link first" href="#">
													<i class="simple-icon-control-start"></i>
												</a>
											</li>-->
											<li class="page-item ">
												<a class="page-link prev" href="#">
													<i class="simple-icon-arrow-left"></i>
												</a>
											</li>
											<!--<li class="page-item active">
												<a class="page-link" href="#">1</a>
											</li>
											<li class="page-item ">
												<a class="page-link" href="#">2</a>
											</li>
											<li class="page-item">
												<a class="page-link" href="#">3</a>
											</li>-->
											<li class="page-item ">
												<a class="page-link next" href="#" aria-label="Next">
													<i class="simple-icon-arrow-right"></i>
												</a>
											</li>
											<!--<li class="page-item ">
												<a class="page-link last" href="#">
													<i class="simple-icon-control-end"></i>
												</a>
											</li>-->
										</ul>
									</nav>
								</div>
								
								
							</div>
						
                   		 </div>
						
						
						
					</div>
					</div>
                </div>
            </div>
        </div>

    </main>

    

    <?php include("incs/js.html") ?>
	
	 <script src="js/vendor/datatables.min.js"></script>
	 
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
$(document).ready(function() {
    $('.select2-normal').select2({
  		//placeholder: 'Content Language',
		minimumResultsForSearch: -1,
		width: 350
	});
} );
	</script>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(2)").addClass('active');
$(".sub-menu .list-unstyled:nth-child(2)>li:nth-child(1)").addClass('active');
</script>
</body>

</html>