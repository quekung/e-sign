<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-default show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1>E-Sign</h1>
						<!--<div class="top-right-button-container">
                        <button type="button" class="btn btn-success top-right-button rounded-05 mr-1"><i class="icon-img"><img src="di/ic-add-plus.png" height="16"></i> MANUAL KEY-IN</button>
						<button type="button" class="btn btn-blue top-right-button rounded-05 mr-1"><i class="icon-img"><img src="di/ic-upload.png" height="16"></i> UPLOAD  FILE</button>
                    </div>-->
					
						<nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>-->
								<li class="breadcrumb-item">
									<a href="#">Import E-Sign</a>
								</li>
								<li class="breadcrumb-item active text-gray" aria-current="page">Upload Document</li>
								
							</ol>
						</nav>

                    </div>


					<div class="card mb-3">
					<div class="card-body p-3">

						<div class="row-dl-upload d-flex flex-wrap justify-content-between align-items-center">
                                <p class="mb-1 col-12 col-sm"><img class="mt-n3 mr-2" src="di/ic-download-fromupload.png" height="40"></i> <span class="d-inline-block mt-2 text-muted">ดาวน์โหลดไฟล์ตัวอย่าง เทมเพลท  อัพโหลด</span></p>
                                <a href="import-estamp-upload-list.php" class="btn btn-success rounded-05 btn-multiple-state" id="successButton">
                                    <div class="spinner d-inline-block">
                                        <div class="bounce1"></div>
                                        <div class="bounce2"></div>
                                        <div class="bounce3"></div>
                                    </div>
                                    <span class="icon success" data-toggle="tooltip" data-placement="top" title="" data-original-title="Everything went right!">
                                        <i class="simple-icon-check"></i>
                                    </span>
                                    <span class="icon fail" data-toggle="tooltip" data-placement="top" title="" data-original-title="Something went wrong!">
                                        <i class="simple-icon-exclamation"></i>
                                    </span>
                                    <span class="label"><i class="icon-img f-white mt-1n"><img src="di/ic-download-fromupload.png" height="16"></i> PDF</span>
                                </a>
                            </div>

                    
						
						
						
					</div>
					</div>
					
					<div class="card mb-3">
					<div class="card-body p-3">
							<div class="wrap-drop mb-2 border-dash">
								<form action="/file-upload">
									<div class="dropzone">
									</div>
								</form>
							</div>
							<p class="mb-0 d-flex flex-wrap justify-content-between">
								<a href="import-estamp-upload-list.php" class="text-primary">tmb_2020_loan_agreement.pdf</a>
								<span><i class="icon-img"><img src="di/ic-check.png" height="14"></i><!--<i class="glyph-icon simple-icon-close"></i>--></span>
								<small class="d-black col-12 p-0 text-light">278 KB</small>
							</p>
							
					</div>
					</div>
					
					
					
                </div>
            </div>
        </div>

    </main>

    

    <?php include("incs/js.html") ?>
	
	 <script src="js/vendor/datatables.min.js"></script>
	 
	 <script src="js/vendor/dropzone.min.js"></script>
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
$(document).ready(function() {
    $('.select2-normal').select2({
  		//placeholder: 'Content Language',
		minimumResultsForSearch: -1,
		width: 350
	});
} );
	</script>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(1)").addClass('active');
$(".sub-menu .list-unstyled:nth-child(1)>li").removeClass('active');
$(".sub-menu .list-unstyled:nth-child(1)>li:nth-child(4)").addClass('active');
</script>
</body>

</html>