<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1>E-Sign</h1>
						<div class="top-right-button-container">
						<a href="javascript:;" data-toggle="modal" data-target="#addScheduleModal" class="btn btn-success top-right-button rounded-05 mr-1"><i class="icon-img"><img src="di/ic-add-plus.png" height="16"></i> Add Schedule</a>
						

                    </div>
					
						<nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>
								<li class="breadcrumb-item">
									<a href="#">Library</a>
								</li>-->
								<li class="breadcrumb-item active text-gray" aria-current="page">Schedule Management</li>
							</ol>
						</nav>

                    </div>
					
					<div class="srh-bar mb-4 d-flex justify-content-between flex-row flex-nowrap">
						<div class="col p-0 pl-1 pr-0">
							<a class="btn p-2 d-inline-block d-md-none" data-toggle="collapse" href="#searchOptions" role="button" aria-expanded="true" aria-controls="searchOptions">
								Display Options
								<i class="simple-icon-arrow-down align-middle"></i>
							</a>
							<div class="card collapse d-md-block" id="searchOptions">
								<div class="card-body p-3 d-flex flex-wrap justify-content-between h-100 align-items-end">

									
									<div class="col mb-1">
											
											<div class="row mb-0 align-items-center">
												

												<div class="col-12 col-sm pr-2 form-group mb-1">
													<label>Schedule Name</label>
													<input class="form-control rounded-05 form-control-sm" placeholder="Specify Schedule Name">
												</div>

												<div class="col-12 col-sm pr-2 form-group mb-1">
													<label>Status</label>
													<select class="form-control select2-normal" data-width="100%" data-placeholder="Select Status">
														<option></option>
														<option>option 1</option>
														<option>option 2</option>
													</select>
												</div>
												
												<div class="col-12 col-sm pr-2 form-group mb-1">
													<label>Application Name</label>
													<select class="form-control select2-normal" data-width="100%" data-placeholder="Select Application Name">
														<option></option>
														<option>option 1</option>
														<option>option 2</option>
													</select>

												</div>
												
											</div>
									</div>
									
									

																		
									<div class="top-right-button-container text-nowrap col-12 col-sm-auto mb-2">
										
										<button class="btn btn-primary btn-md top-right-button rounded-05" type="button" id="btnSearch" style="min-width: 120px"> <i class="icon-img"><img src="di/ic-search-wh.png" height="20"></i> Search</button>
									</div>

								</div>
							</div>
						</div>

					</div>


					<div class="card">
					<div class="card-body p-0">
						<div class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">

                           <!--<table class="data-table dataTable no-footer responsive nowrap table-responsive-lg" >-->
						   <table id="scrollx" class=" skin-tmb">

							<thead>
								
								<tr>
<th class="text-white sort-none">No.</th>
<th class="text-white sort-none">Schedule Name</th>
<th class="text-white sort-none">Application Name</th>
<th class="text-white sort-none">Application Task</th>
<th class="text-white sort-none">Cron</th>
<th class="text-white sort-none">Description</th>
<th class="text-white sort-none">Certificate</th>


<th class="text-white sort-none">Signature</th>
<th class="text-white sort-none">Fomat File</th>
<th class="text-white sort-none">Status</th>
<th class="text-white sort-none">Start Effective Date</th>
<th class="text-white sort-none">End Effective Date</th>
<th class="text-white sort-none text-center">Action</th>
</tr>

							</thead>
							<tbody>
								<?php for($i=1;$i<=10;$i++){ ?>
								<tr>
									<td><?php echo $i; ?></td>
									<td>
									<?php if($i%3==0){ ?>Realtime Sign Load of money
									<?php } else { ?>Daily Sign Load of money
									<?php } ?></td>
									<td>
									<?php if($i%2==0){ ?>ALS
									<?php } elseif($i%3==0){ ?>TAP
									<?php } else { ?>ESTMP
									<?php } ?></td>
									<td>ERD001</td>								
									<td>5 4 * * *</td>
									<td>At 14:15 on day-of-month 1.</td>
									<td>TMB.co.th</td>
									
									<td>ALS-Loan of money</td>
							
									<td>PDFA3</td>
									
									
									<td><?php if($i>2 && $i<6){ ?><span class="text-black-50">Inactive</span><?php } else { ?> <span class="text-success">Active</span> <?php } ?></td>
									<td>15/10/20</td>
									<td>15/10/21</td>
									<td class="text-center">
											<a href="#" class="btn btn-xs " title="Edit"><i class="icon-img"><img src="di/ic-edit.png" height="16"></i></a>
									</td>

									
								</tr>
								<?php } ?>


							</tbody>
						</table>
								
		

						</div>
						
						<div class="mt-4">
                 
						<div class="d-flex justify-content-end align-items-center">
								<div class="dropdown-as-select display-page" id="pageCount">
									<span class="text-light text-small">Rows per page </span>
									<button class="btn btn-outline-dark btn-xs dropdown-toggle" type="button"
										data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										10
									</button>
									<div class="dropdown-menu dropdown-menu-right">
										<a class="dropdown-item" href="#">5</a>
										<a class="dropdown-item active" href="#">10</a>
										<a class="dropdown-item" href="#">20</a>
									</div>
								</div>
								<div class="d-block d-md-inline-block ml-5">
									<nav class="ctrl-page d-flex flex-nowrap align-items-center">
										<span> 1-10 of 40</span>
										<ul class="pagination justify-content-center mb-0">
										   <!-- <li class="page-item ">
												<a class="page-link first" href="#">
													<i class="simple-icon-control-start"></i>
												</a>
											</li>-->
											<li class="page-item ">
												<a class="page-link prev" href="#">
													<i class="simple-icon-arrow-left"></i>
												</a>
											</li>
											<!--<li class="page-item active">
												<a class="page-link" href="#">1</a>
											</li>
											<li class="page-item ">
												<a class="page-link" href="#">2</a>
											</li>
											<li class="page-item">
												<a class="page-link" href="#">3</a>
											</li>-->
											<li class="page-item ">
												<a class="page-link next" href="#" aria-label="Next">
													<i class="simple-icon-arrow-right"></i>
												</a>
											</li>
											<!--<li class="page-item ">
												<a class="page-link last" href="#">
													<i class="simple-icon-control-end"></i>
												</a>
											</li>-->
										</ul>
									</nav>
								</div>
								
								
							</div>
						
                   		 </div>
						
						
						
					</div>
					</div>
                </div>
            </div>
        </div>

    </main>

<!-- Modal Add -->
<div class="modal fade" id="addScheduleModal" tabindex="-1" role="dialog"	aria-hidden="true">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title text-primary">Add Schedule</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body pt-0 pb-0 text-left">
			   <div class="row mt-4">
			   	  <div class="col-12 mb-0 form-group">
					 <label>Schedule Name*</label>
					 <input class="form-control rounded-05 form-control-sm" placeholder="Specify Schedule Name">
				   </div>
			   </div>
			   <div class="row">
			   	  <div class="col-12 mt-3 mb-0 form-group">
					 <label>Application Name*</label>
					 <select class="form-control select2-normal" data-width="100%" data-placeholder="Select Application Name">
						<option></option>
						<option>Option1</option>
						<option>Option2</option>
					 </select>
				   </div>
			   </div>
			   
			   <div class="row">
			   	  <div class="col-6 mt-3 mb-0 form-group">
					 <label>Format File*</label>
					 <select class="form-control select2-normal" data-width="100%" data-placeholder="Select Format File">
						<option></option>
						<option>Option1</option>
						<option>Option2</option>
					 </select>
				   </div>
			   	  <div class="col-6 mt-3 mb-0 form-group">
					 <label>Certificate Name*</label>
					 <select class="form-control select2-normal" data-width="100%" data-placeholder="Select Certificate Name">
						<option></option>
						<option>Option1</option>
						<option>Option2</option>
					 </select>
				   </div>
			   </div>
			   
			   <div class="row">
			   	  <div class="col-6 mt-3 mb-0 form-group">
					 <label>Signature*</label>
					 <select class="form-control select2-normal" data-width="100%" data-placeholder="Select Signature">
						<option></option>
						<option>Option1</option>
						<option>Option2</option>
					 </select>
				   </div>
			   	  <div class="col-6 mt-3 mb-0 form-group">
					 <label>Callback URL</label>
					 <input class="form-control rounded-05 form-control-sm" placeholder="Specify Callback URL">
				   </div>
			   </div>
			   
			   <div class="row">
			   		<div class="col-6 mt-3 mb-0 form-group">
						 <label>Start Effective Date*</label>
						 <div class="input-group date">
								<span class="input-group-text input-group-append input-group-addon border-right-0">
									<i class="simple-icon-calendar font-medium"></i>
								</span>
								<input type="text" class="input-sm form-control border-left-0" name="Expire" placeholder="DD/MM/YY">
							</div>
					</div>
					<div class="col-6 mt-3 mb-0 form-group">
					 	<label>Status</label>
						 <select class="form-control select2-normal" data-width="100%" data-placeholder="Select Status">
							<option></option>
							<option>Option1</option>
							<option>Option2</option>
						 </select>
					</div>
			   </div>
			   
			   <div class="row">
			   	  <div class="col-6 mt-3 mb-0 form-group">
					 <label>Cron*</label>
					 <input class="form-control rounded-05 form-control-sm" placeholder="Specify Cron">
				   </div>
			   	  <div class="col-6 mt-3 mb-0 form-group">
					 <label>Description</label>
					 <input class="form-control rounded-05 form-control-sm" placeholder="Specify Description">
				   </div>
			   </div>

			  </div>
			  
			  <div class="modal-footer pt-4 d-flex justify-content-center border-0">
				<button type="button" class="btn btn-md btn-outline-dark rounded-05 col-3 mr-4" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-md btn-primary rounded-05 col-3 ml-4" data-dismiss="modal" data-toggle="modal" data-target="#Modalsuccess">Submit</button>
			  </div>
			  
		</div>
	</div>
</div>
			   
    

    <?php include("incs/js.html") ?>
	<?php include("incs/popup.html") ?>
	 <script src="js/vendor/datatables.min.js"></script>
	 
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
$(document).ready(function() {
    $('.select2-normal').select2({
  		//placeholder: 'Content Language',
		minimumResultsForSearch: -1,
		width: 350
	});
	//dataTable
    $('#scrollx').DataTable( {
		searching: false,
        bLengthChange: false,
        destroy: true,
        info: false,
        paging: false,
        //sDom: '<"row view-filter"<"col-sm-12"<"float-left"l><"float-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
        //responsive: !0,
       //deferRender: !0,
        //scrollY: "calc(100vh - 400px)",
        scrollCollapse: !0,
        scrollX: true
    } );
} );
	</script>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(1)").addClass('active');
$(".sub-menu .list-unstyled:nth-child(1)>li:nth-child(5)").addClass('active');
</script>
</body>

</html>