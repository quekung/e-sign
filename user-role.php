<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-default show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1>E-Sign</h1>
						<div class="top-right-button-container">
                        <a href="javascript:;" data-toggle="modal" data-target="#addRoleModal" class="btn btn-success top-right-button rounded-05 mr-1"><i class="icon-img"><img src="di/ic-add-plus.png" height="16"></i> Add Role</a>
						

                        <!--<div class="btn-group">
                            <div class="btn btn-primary btn-lg pl-4 pr-0 check-button">
                                <label class="custom-control custom-checkbox mb-0 d-inline-block">
                                    <input type="checkbox" class="custom-control-input" id="checkAllDataTables">
                                    <span class="custom-control-label">&nbsp;</span>
                                </label>
                            </div>
                            <button type="button" class="btn btn-lg btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                            </div>
                        </div>-->
                    </div>
					
						<nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>
								<li class="breadcrumb-item">
									<a href="#">Library</a>
								</li>-->
								<li class="breadcrumb-item active text-gray" aria-current="page">Role Management</li>
							</ol>
						</nav>

                    </div>


					<div class="card">
					<div class="card-body p-0">
						<div class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">

                           <!--<table class="data-table dataTable no-footer responsive nowrap table-responsive-lg" >-->
						   <table  class="data-table data-table-scrollable responsive nowrap skin-tmb">
							
									<thead>
										<tr>
											<th class="text-white sort-none">No.</th>
											<th class="text-white sort-none">Role Name</th>		
											<!--<th class="text-white sort-none">Description</th>-->
											<th class="text-white sort-none">Status</th>
											<th class="text-white text-center sort-none">Action</th>

										</tr>
									</thead>
									<tbody>
										<?php for($i=1;$i<=10;$i++){ ?>
										<tr>
											<td><?php echo $i ?></td>
											<td><?php if($i==2){ ?>TMBOA_Application Security Manager
											<?php } elseif($i==3){ ?>TMBOA_IT Operation
											<?php } elseif($i==4){ ?>TMBOA_Operation Maker
											<?php } elseif($i==5){ ?>TMBOA_Operation Approver
											<?php } elseif($i==6){ ?>TMBOA_Product Approver
											<?php } else { ?>TMBOA_Application User Manager
											<?php } ?>
											</td>
											<!--<td><?php if($i==8){ ?>-<?php } elseif($i>2 && $i<6){ ?>Senior IT Security Associate <?php } else { ?>Senior Operations and Services Associate <?php } ?></td>-->
											<td><?php if($i>2 && $i<6){ ?><span class="text-black-50">Inactive</span><?php } else { ?> <span class="text-success">Active</span> <?php } ?></td>
											<td class=" text-center">
												<a href="javascript:;" data-toggle="modal" data-target="#editRoleModal" class="btn  btn-xs p-1 active" title="EditRole"><i class="icon-img"><img src="di/ic-edit.png" height="16"></i></a>
											</td>
											
										</tr>
										<?php } ?>
							

									</tbody>
								</table>
								
		

						</div>
						
						<div class="mt-4">
                 
						<div class="d-flex justify-content-end align-items-center">
								<div class="dropdown-as-select display-page" id="pageCount">
									<span class="text-light text-small">Rows per page </span>
									<button class="btn btn-outline-dark btn-xs dropdown-toggle" type="button"
										data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										10
									</button>
									<div class="dropdown-menu dropdown-menu-right">
										<a class="dropdown-item" href="#">5</a>
										<a class="dropdown-item active" href="#">10</a>
										<a class="dropdown-item" href="#">20</a>
									</div>
								</div>
								<div class="d-block d-md-inline-block ml-5">
									<nav class="ctrl-page d-flex flex-nowrap align-items-center">
										<span> 1-10 of 40</span>
										<ul class="pagination justify-content-center mb-0">
										   <!-- <li class="page-item ">
												<a class="page-link first" href="#">
													<i class="simple-icon-control-start"></i>
												</a>
											</li>-->
											<li class="page-item ">
												<a class="page-link prev" href="#">
													<i class="simple-icon-arrow-left"></i>
												</a>
											</li>
											<!--<li class="page-item active">
												<a class="page-link" href="#">1</a>
											</li>
											<li class="page-item ">
												<a class="page-link" href="#">2</a>
											</li>
											<li class="page-item">
												<a class="page-link" href="#">3</a>
											</li>-->
											<li class="page-item ">
												<a class="page-link next" href="#" aria-label="Next">
													<i class="simple-icon-arrow-right"></i>
												</a>
											</li>
											<!--<li class="page-item ">
												<a class="page-link last" href="#">
													<i class="simple-icon-control-end"></i>
												</a>
											</li>-->
										</ul>
									</nav>
								</div>
								
								
							</div>
						
                   		 </div>
						
						
						
					</div>
					</div>
					
                </div>
            </div>
        </div>

    </main>

<!-- Modal Add -->
<div class="modal fade" id="addRoleModal" tabindex="-1" role="dialog"	aria-hidden="true">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title text-primary">Add Role</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body pt-0 pb-0 text-left">
			   <div class="row mt-4">
			   	  <div class="col-12 form-group">
					 <label>Role Name *</label>
					 <input class="form-control rounded-05 form-control-sm" placeholder="Select Role Name*">
					 <!--<div class="input-group date">
						<span class="input-group-text input-group-append input-group-addon">
							<i class="simple-icon-calendar"></i>
						</span>
						<input type="text" class="input-sm form-control" name="start" placeholder="Start date" value="08/06/2020" />
					</div>-->
				   </div>
			   </div>
			  
			  <div class="wrap-role-user">

					   <!--<table class="data-table dataTable no-footer responsive nowrap table-responsive-lg" >-->
					   <table class="table table-responsive-xs">
                                <thead>
									<tr>
										<th class="text-black-50 border-bottom-0 border-top-0">&nbsp;</th>
										<th class="text-black-50 text-center pl-0 border-bottom-0 border-top-0">Add</th>
										<th class="text-black-50 text-center pl-0 border-bottom-0 border-top-0">Edit</th>
										<th class="text-black-50 text-center pl-0 pr-0 border-bottom-0 border-top-0">View</th>

									</tr>
								</thead>
								<tbody>
									<?php for($i=1;$i<=4;$i++){ ?>
									<tr>
										<td class="pb-2 border-bottom-0">
											<label>
			
												<?php if($i==1){ ?>Certificate Management
												<?php } elseif($i==2){ ?>Signature Management
												<?php } elseif($i==3){ ?>Schedule Management
												<?php } elseif($i==4){ ?>Document List
												<?php } ?>

											</label>
										</td>
										
										
										<td class="text-center pl-0 pb-0 border-bottom-0">
											<label class="custom-control custom-checkbox mb-0 mr-n3 d-inline-block">
											<input type="checkbox" class="custom-control-input">
											<span class="custom-control-label">&nbsp;</span>
											</label>
										</td>
										<td class="text-center pl-0 pb-0 border-bottom-0">
											<label class="custom-control custom-checkbox mb-0 mr-n3 d-inline-block">
												<input type="checkbox" class="custom-control-input">
												<span class="custom-control-label">&nbsp;</span>
											</label>
										</td>
										<td class="text-center pl-0 pr-0 pb-0 border-bottom-0">
											<label class="custom-control custom-checkbox mb-0 mr-n3 d-inline-block">
												<input type="checkbox" class="custom-control-input">
												<span class="custom-control-label">&nbsp;</span>
											</label>
										</td>
									</tr>
									<?php } ?>
									
									
									
									
								</tbody>
						</table>
			</div>
			   
			</div>
			<div class="modal-footer pt-3 d-flex justify-content-center border-0">
				<button type="button" class="btn btn-md btn-outline-dark rounded-05 col-3 mr-4" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-md btn-primary rounded-05 col-3 ml-4" data-dismiss="modal" data-toggle="modal" data-target="#payModalsuccess">Submit</button>
			</div>
		</div>
	</div>
</div>
<!-- /Modal -->
<!-- Modal Edit -->
<div class="modal fade" id="editRoleModal" tabindex="-1" role="dialog"	aria-hidden="true">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title text-primary">TMBOA Application User Manager</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body pt-0 pb-0 text-left">
		  
			  <div class="wrap-role-user">

					   <!--<table class="data-table dataTable no-footer responsive nowrap table-responsive-lg" >-->
					   <table class="table table-responsive-xs">
                                <thead>
									<tr>
										<th class="text-black-50 border-bottom-0">&nbsp;</th>
										<th class="text-black-50 text-center pl-0 border-bottom-0">Add</th>
										<th class="text-black-50 text-center pl-0 border-bottom-0">Edit</th>
										<th class="text-black-50 text-center pl-0 pr-0 border-bottom-0">View</th>

									</tr>
								</thead>
								<tbody>
									<?php for($i=1;$i<=4;$i++){ ?>
									<tr>
										<td class="pb-2 border-bottom-0">
											<label>
			
												<?php if($i==1){ ?>Certificate Management
												<?php } elseif($i==2){ ?>Signature Management
												<?php } elseif($i==3){ ?>Schedule Management
												<?php } elseif($i==4){ ?>Document List
												<?php } ?>

											</label>
										</td>
										
										
										<td class="text-center pl-0 pb-0 border-bottom-0">
											<label class="custom-control custom-checkbox mb-0 mr-n3 d-inline-block">
											<input type="checkbox" class="custom-control-input">
											<span class="custom-control-label">&nbsp;</span>
											</label>
										</td>
										<td class="text-center pl-0 pb-0 border-bottom-0">
											<label class="custom-control custom-checkbox mb-0 mr-n3 d-inline-block">
												<input type="checkbox" class="custom-control-input">
												<span class="custom-control-label">&nbsp;</span>
											</label>
										</td>
										<td class="text-center pl-0 pr-0 pb-0 border-bottom-0">
											<label class="custom-control custom-checkbox mb-0 mr-n3 d-inline-block">
												<input type="checkbox" class="custom-control-input">
												<span class="custom-control-label">&nbsp;</span>
											</label>
										</td>
									</tr>
									<?php } ?>
									
									
									
									
								</tbody>
						</table>
			</div>
			 
			   
			</div>
			<div class="modal-footer pt-3 d-flex justify-content-center border-0">
				<button type="button" class="btn btn-md btn-outline-dark rounded-05 col-3 mr-4" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-md btn-primary rounded-05 col-3 ml-4" data-dismiss="modal" data-toggle="modal" data-target="#payModalsuccess">Submit</button>
			</div>
		</div>
	</div>
</div>
<!-- /Modal -->


    <?php include("incs/js.html") ?>
	
	 <script src="js/vendor/datatables.min.js"></script>
	 
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
$(document).ready(function() {
    $('.select2-normal').select2({
  		//placeholder: 'Content Language',
		minimumResultsForSearch: -1,
		width: 350
	});
} );
	</script>
		<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(3)").addClass('active');
//$(".sub-menu .list-unstyled:nth-child(4)>li:nth-child(3)").addClass('active');
</script>
</body>

</html>