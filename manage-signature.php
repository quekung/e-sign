<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1>E-Sign</h1>
						<div class="top-right-button-container">
						<a href="javascript:;" data-toggle="modal" data-target="#addSignatureModal" class="btn btn-success top-right-button rounded-05 mr-1"><i class="icon-img"><img src="di/ic-add-plus.png" height="16"></i> Add Signature</a>
						

                    </div>
					
						<nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>
								<li class="breadcrumb-item">
									<a href="#">Library</a>
								</li>-->
								<li class="breadcrumb-item active text-gray" aria-current="page">Signature Management</li>
							</ol>
						</nav>

                    </div>

                    <div class="srh-bar mb-4 d-flex justify-content-between flex-row flex-nowrap">
						<div class="col p-0 pl-1 pr-0">
							<a class="btn p-2 d-inline-block d-md-none" data-toggle="collapse" href="#searchOptions"
								role="button" aria-expanded="true" aria-controls="searchOptions">
								Display Options
								<i class="simple-icon-arrow-down align-middle"></i>
							</a>
							<div class="collapse d-md-block" id="searchOptions">
								<div class="d-flex flex-wrap justify-content-between h-100">
									<div class="main-srh col-xs-12 col-sm p-0 mr-3 mb-1">

										<div class="input-group">
											
											<input type="text" class="form-control bg-transparent" placeholder="Search by  Name ,  Application ID ,  Application Name">
											<div class="input-group-append position-absolute">
												<button type="button" class="btn bg-transparent border-0"><img src="di/ic-search.png" height="22"></button>
											</div>
										</div>
									</div>

																		
									<div class="float-md-left d-flex align-items-center">
										<div class="mr-1">
											<label class="hid">Status</label>
											<select class="form-control select2-normal" data-width="120" data-placeholder="Status">
												<option></option>
												<option>Active</option>
												<option>Inactive</option>
											</select>
										</div>
										<a href="signature-upload.php" class="btn top-right-button rounded-05 mr-1 btn-outline-blue"><i class="icon-img"><img src="di/ic-upload-doc.png" height="16"></i> Upload Document <small class="d-block mt-3 text-gray position-absolute">*Support format file PDF/A3</small></a>

										
									</div>

								</div>
							</div>
						</div>
						
					</div>


					<div class="card">
					<div class="card-body p-0">
						<div class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">

                           <!--<table class="data-table dataTable no-footer responsive nowrap table-responsive-lg" >-->
						   <table  class="data-table data-table-scrollable responsive nowrap skin-tmb">

							<thead>
								
								<tr>
<th rowspan="2" class="text-white sort-none">No.</th>
<th rowspan="2" class="text-white sort-none">Name</th>
<th rowspan="2" class="text-white sort-none">Application Task</th>
<th rowspan="2" class="text-white sort-none pr-0">Application Name</th>
<th colspan="2" class="text-white sort-none pt-0 pb-0 text-center">Position</th>
<th rowspan="2" class="text-white sort-none pl-4">Size</th>
<th rowspan="2" class="text-white sort-none text-center">Status</th>	
<th rowspan="2" class="text-white sort-none">Start Effective <br>Date</th>
<th rowspan="2" class="text-white sort-none">End Effective <br>Date</th>	
<th rowspan="2" class="text-white sort-none text-center">Action</th>
</tr>
<tr>
	<th class="text-white sort-none bg-gray2 pt-0 pb-0 pl-3 pr-3 text-center">X</th>
	<th class="text-white sort-none bg-gray2 pt-0 pb-0 pl-3 pr-3 text-center">Y</th>	


</tr>
							</thead>
							<tbody>
								<?php for($i=1;$i<=10;$i++){ ?>
								<tr>
									
									<td><?php echo $i ?></td>
									<td><?php if($i%3==0){ ?>ALS-Loan of money<?php } else { ?>TAP-Authorization letter<?php } ?></td>
									<td>ERD001</td>								
									<td>E-Stamp</td>
									<td class="text-center">200</td>
									<td class="text-center">300</td>
									<td class="pl-4">50x150</td>
									<td class="pl-2"><?php if($i>2 && $i<6){ ?><span class="text-black-50">Inactive</span><?php } else { ?> <span class="text-success">Active</span> <?php } ?></td>
									<td>7/10/20</td>
									<td>7/10/20</td>
									<td class="text-center">
											<a href="#" class="btn  btn-xs p-1" title="View"><i class="icon-img"><img src="di/ic-view.png" height="16"></i></a> 
											<a href="#" class="btn  btn-xs p-" title="Edit"><i class="icon-img"><img src="di/ic-edit.png" height="16"></i></a>
									</td>
									


									
									


								</tr>
								<?php } ?>


							</tbody>
						</table>
								
		

						</div>
						
						<div class="mt-4">
                 
						<div class="d-flex justify-content-end align-items-center">
								<div class="dropdown-as-select display-page" id="pageCount">
									<span class="text-light text-small">Rows per page </span>
									<button class="btn btn-outline-dark btn-xs dropdown-toggle" type="button"
										data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										10
									</button>
									<div class="dropdown-menu dropdown-menu-right">
										<a class="dropdown-item" href="#">5</a>
										<a class="dropdown-item active" href="#">10</a>
										<a class="dropdown-item" href="#">20</a>
									</div>
								</div>
								<div class="d-block d-md-inline-block ml-5">
									<nav class="ctrl-page d-flex flex-nowrap align-items-center">
										<span> 1-10 of 40</span>
										<ul class="pagination justify-content-center mb-0">
										   <!-- <li class="page-item ">
												<a class="page-link first" href="#">
													<i class="simple-icon-control-start"></i>
												</a>
											</li>-->
											<li class="page-item ">
												<a class="page-link prev" href="#">
													<i class="simple-icon-arrow-left"></i>
												</a>
											</li>
											<!--<li class="page-item active">
												<a class="page-link" href="#">1</a>
											</li>
											<li class="page-item ">
												<a class="page-link" href="#">2</a>
											</li>
											<li class="page-item">
												<a class="page-link" href="#">3</a>
											</li>-->
											<li class="page-item ">
												<a class="page-link next" href="#" aria-label="Next">
													<i class="simple-icon-arrow-right"></i>
												</a>
											</li>
											<!--<li class="page-item ">
												<a class="page-link last" href="#">
													<i class="simple-icon-control-end"></i>
												</a>
											</li>-->
										</ul>
									</nav>
								</div>
								
								
							</div>
						
                   		 </div>
						
						
						
					</div>
					</div>
                </div>
            </div>
        </div>

    </main>

<!-- Modal Add -->
<div class="modal fade" id="addSignatureModal" tabindex="-1" role="dialog"	aria-hidden="true">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title text-primary">Add Signature</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body pt-0 pb-0 text-left">
			   <div class="row mt-4">
			   	  <div class="col-12 mb-0form-group">
					 <label>Name *</label>
					 <input class="form-control rounded-05 form-control-sm" placeholder="Specify Name">
					 <!--<div class="input-group date">
						<span class="input-group-text input-group-append input-group-addon">
							<i class="simple-icon-calendar"></i>
						</span>
						<input type="text" class="input-sm form-control" name="start" placeholder="Start date" value="08/06/2020" />
					</div>-->
				   </div>
			   </div>
			   <div class="row">
			   		<div class="col-6 mt-2 mb-0 form-group">
						 <label>Application* </label>
						 <select class="form-control select2-normal" data-width="100%" data-placeholder="Select Application">
							<option></option>
							<option>Option1</option>
							<option>Option2</option>
						 </select>
					</div>
					<div class="col-6 mt-2 mb-0 form-group">
					 	<label>Application Task*</label>
						 <select class="form-control select2-normal" data-width="100%" data-placeholder="Select Application Task">
							<option></option>
							<option>Option1</option>
							<option>Option2</option>
						 </select>
					</div>
			   </div>
			   <div class="row">
			   		<div class="col-6 col-sm-8 mt-2 mb-0 form-group">
						 <label>Page Style*</label>
						 <select class="form-control select2-normal" data-width="100%" data-placeholder="Select Page Style*">
							<option></option>
							<option>Option1</option>
							<option>Option2</option>
						 </select>
					</div>
					<div class="col-6 col-sm-4 mt-2 mb-0 form-group">
					 	<label>Page</label>
						 <select class="form-control select2-normal" data-width="100%" data-placeholder="Select">
							<option></option>
							<option>Option1</option>
							<option>Option2</option>
						 </select>
					</div>
			   </div>
			   <div class="separator mt-4 mb-4"></div>
			   <h5 class="mb-2 text-small">Preview Signature</h5>
			   <div class="row sign-upload">
			   		<div class="col-sm-6 preview h-100">
						<figure class="d-flex h-100 justify-content-center align-items-center">
							<img class="img-thumbnail" src="di/ic-signature.png" alt="">
						</figure>
					</div>
					<div class="col-sm-6">
						<div class="form-group wrap-drop">
						 <label class="font-weight-light">Browse (dimension 50x150 px)</label>
						 <input type="file" class="form-control rounded-05 form-control-sm" placeholder="C:/Image">
						 <small class="text-gray">Format JPG,PNG max size 10 kb</small>
						</div>
						<p class="mb-0 d-flex flex-wrap justify-content-between">
							<a href="import-estamp-upload-list.php" class="text-primary">Signature.jpg</a>
							<span><i class="icon-img"><img src="di/ic-check.png" height="12"></i><!--<i class="glyph-icon simple-icon-close"></i>--></span>
							<small class="d-black col-12 p-0 text-light">278 KB</small>
						</p>
					</div>
			   </div>
			  </div>
			  
			  <div class="modal-footer pt-3 d-flex justify-content-center border-0">
				<button type="button" class="btn btn-md btn-outline-dark rounded-05 col-3 mr-4" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-md btn-primary rounded-05 col-3 ml-4" data-dismiss="modal" data-toggle="modal" data-target="#addModalsuccess">Submit</button>
			  </div>
			  
		</div>
	</div>
</div>
			   
    

    <?php include("incs/js.html") ?>
	<?php include("incs/popup.html") ?>
	 <script src="js/vendor/datatables.min.js"></script>
	 
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
$(document).ready(function() {
    $('.select2-normal').select2({
  		//placeholder: 'Content Language',
		minimumResultsForSearch: -1,
		width: 350
	});
} );
	</script>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(1)").addClass('active');
$(".sub-menu .list-unstyled:nth-child(1)>li:nth-child(4)").addClass('active');
</script>
</body>

</html>