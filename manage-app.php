<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1>E-Sign</h1>
						<div class="top-right-button-container">
						<a href="javascript:;" data-toggle="modal" data-target="#addAppModal" class="btn btn-success top-right-button rounded-05 mr-1"><i class="icon-img"><img src="di/ic-add-plus.png" height="16"></i> Add Application</a>
						

                    </div>
					
						<nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>
								<li class="breadcrumb-item">
									<a href="#">Library</a>
								</li>-->
								<li class="breadcrumb-item active text-gray" aria-current="page">Application Management</li>
							</ol>
						</nav>

                    </div>


					<div class="card">
					<div class="card-body p-0">
						<div class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">

                           <!--<table class="data-table dataTable no-footer responsive nowrap table-responsive-lg" >-->
						   <table  class="data-table data-table-scrollable responsive nowrap skin-tmb">

							<thead>
								
								<tr>
<th class="text-white sort-none">No.</th>
<th class="text-white sort-none">Abbreviate</th>
<th class="text-white sort-none">Name</th>
<th class="text-white sort-none">Description</th>
<th class="text-white sort-none">IP Address</th>
<th class="text-white sort-none">Status</th>
<th class="text-white sort-none">Create Date</th>
<th class="text-white sort-none text-center">Action</th>
</tr>

							</thead>
							<tbody>
								<?php for($i=1;$i<=10;$i++){ ?>
								<tr>
									
									<td>00<?php echo $i ?></td>

									<td>ESTMP</td>								
									<td>ABCD123455<?php echo $i ?></td>
									<td>-</td>
									<td>10.1.1.155</td>
									<td><?php if($i>2 && $i<6){ ?><span class="text-black-50">Inactive</span><?php } else { ?> <span class="text-success">Active</span> <?php } ?></td>
									<td>15/10/20 17.00</td>
									<td class="text-center">
											
											<a href="#" class="btn  btn-xs p-1" title="Edit"><i class="icon-img"><img src="di/ic-edit.png" height="16"></i></a>
											<a href="#" class="btn  btn-xs p-1" title="Access"><i class="icon-img"><img src="di/ic-access.png" height="16"></i></a> 
											<a href="#" class="btn  btn-xs p-1" title="Add Task"><i class="icon-img"><img src="di/ic-add-task.png" height="16"></i></a> 
									</td>
									
								</tr>
								<?php } ?>


							</tbody>
						</table>
								
		

						</div>
						
						<div class="mt-4">
                 
						<div class="d-flex justify-content-end align-items-center">
								<div class="dropdown-as-select display-page" id="pageCount">
									<span class="text-light text-small">Rows per page </span>
									<button class="btn btn-outline-dark btn-xs dropdown-toggle" type="button"
										data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										10
									</button>
									<div class="dropdown-menu dropdown-menu-right">
										<a class="dropdown-item" href="#">5</a>
										<a class="dropdown-item active" href="#">10</a>
										<a class="dropdown-item" href="#">20</a>
									</div>
								</div>
								<div class="d-block d-md-inline-block ml-5">
									<nav class="ctrl-page d-flex flex-nowrap align-items-center">
										<span> 1-10 of 40</span>
										<ul class="pagination justify-content-center mb-0">
										   <!-- <li class="page-item ">
												<a class="page-link first" href="#">
													<i class="simple-icon-control-start"></i>
												</a>
											</li>-->
											<li class="page-item ">
												<a class="page-link prev" href="#">
													<i class="simple-icon-arrow-left"></i>
												</a>
											</li>
											<!--<li class="page-item active">
												<a class="page-link" href="#">1</a>
											</li>
											<li class="page-item ">
												<a class="page-link" href="#">2</a>
											</li>
											<li class="page-item">
												<a class="page-link" href="#">3</a>
											</li>-->
											<li class="page-item ">
												<a class="page-link next" href="#" aria-label="Next">
													<i class="simple-icon-arrow-right"></i>
												</a>
											</li>
											<!--<li class="page-item ">
												<a class="page-link last" href="#">
													<i class="simple-icon-control-end"></i>
												</a>
											</li>-->
										</ul>
									</nav>
								</div>
								
								
							</div>
						
                   		 </div>
						
						
						
					</div>
					</div>
                </div>
            </div>
        </div>

    </main>

<!-- Modal Add -->
<div class="modal fade" id="addAppModal" tabindex="-1" role="dialog"	aria-hidden="true">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title text-primary">Add Application</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body pt-0 pb-0 text-left">
			   <div class="row mt-4">
			   	  <div class="col-6 mb-0 form-group">
					 <label>Abbreviate</label>
					 <input class="form-control rounded-05 form-control-sm" placeholder="Specify Application ID">
				   </div>
			   	  <div class="col-6 mb-0 form-group">
					 <label>Name *</label>
					 <input class="form-control rounded-05 form-control-sm" placeholder="Specify Name">
				   </div>
			   </div>
			   <div class="row">
			   		<div class="col-6 mt-2 mb-0 form-group">
						 <label>e-mail</label>
						 <input class="form-control rounded-05 form-control-sm" placeholder="Specify e-mail">
					</div>
					<div class="col-6 mt-2 mb-0 form-group">
						 <label>Description</label>
						 <input class="form-control rounded-05 form-control-sm" placeholder="Specify Description">
					</div>
			   </div>
			   <div class="row">
			   		<div class="col-6 mt-2 mb-0 form-group">
						 <label>IP Address*</label>
						 <input class="form-control rounded-05 form-control-sm" placeholder="Specify IP Address">
					</div>
					<div class="col-6 mt-2 mb-0 form-group">
					 	<label>Status</label>
						 <select class="form-control select2-normal" data-width="100%" data-placeholder="Select Status">
							<option></option>
							<option>Option1</option>
							<option>Option2</option>
						 </select>
					</div>
			   </div>

			  </div>
			  
			  <div class="modal-footer pt-3 d-flex justify-content-center border-0">
				<button type="button" class="btn btn-md btn-outline-dark rounded-05 col-3 mr-4" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-md btn-primary rounded-05 col-3 ml-4" data-dismiss="modal" data-toggle="modal" data-target="#addModalsuccess">Submit</button>
			  </div>
			  
		</div>
	</div>
</div>
			   
    

    <?php include("incs/js.html") ?>
	<?php include("incs/popup.html") ?>
	 <script src="js/vendor/datatables.min.js"></script>
	 
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
$(document).ready(function() {
    $('.select2-normal').select2({
  		//placeholder: 'Content Language',
		minimumResultsForSearch: -1,
		width: 350
	});
} );
	</script>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(1)").addClass('active');
$(".sub-menu .list-unstyled:nth-child(1)>li:nth-child(2)").addClass('active');
</script>
</body>

</html>